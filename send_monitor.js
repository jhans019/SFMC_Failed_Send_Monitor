<script runat="server">
    Platform.Load("Core", "1.1.1");
		/* Clear Data Extension Before Running Script */
    Platform.Function.DeleteData('Error_Reporting', ["Lookup"], ["1"]);
    /* Format Date Value */
    function getFormattedDate(date) {
        var year = date.getFullYear();
        var month = (1 + date.getMonth()).toString();
        month = month.length > 1 ? month : '0' + month;
        var day = date.getDate().toString();
        day = day.length > 1 ? day : '0' + day;
        return month + '-' + day + '-' + year;
    }
    var prev_day = new Date();
    prev_day.setDate(prev_day.getDate() - 1);
    prev_day = getFormattedDate(prev_day);
		/* Begin Retrieve Request */
    var rr = Platform.Function.CreateObject("RetrieveRequest");
    Platform.Function.SetObjectProperty(rr, "ObjectType", "Send");
    Platform.Function.AddObjectArrayItem(rr, "Properties", "Status");
    Platform.Function.AddObjectArrayItem(rr, "Properties", "EmailName");
    Platform.Function.AddObjectArrayItem(rr, "Properties", "SentDate");
		/* Filter Response */
    var sfp = Platform.Function.CreateObject("SimpleFilterPart");
    Platform.Function.SetObjectProperty(sfp, "Property", "SentDate");
    Platform.Function.SetObjectProperty(sfp, "SimpleOperator", "greaterThan");
    Platform.Function.AddObjectArrayItem(sfp, "Value", prev_day);
    Platform.Function.SetObjectProperty(rr, "Filter", sfp);
		/* Invoke Retrieve */
    var rows = Platform.Function.InvokeRetrieve(rr, results);
    var check_date = new Date();
    check_date.setHours(check_date.getHours() - 1);
    for (i = 0; i <= rows.length; ++i) {
        var send_status = rows[i].Status;
        var sent_date = rows[i].SentDate;
        var format_sent = new Date(sent_date);
        var format_check = new Date(check_date);
        var name = rows[i].EmailName;
        if ((send_status == "Canceled" || send_status == "Failed" || send_status == "Paused") && format_sent >= format_check) {
            Platform.Function.InsertData("Error_Reporting", ["EmailName", "JobStatus", "SchedTime"], [name, send_status, format_sent]);
        }
    }
</script>